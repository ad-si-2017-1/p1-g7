  exports.broadcast = function(message, sender, clients) {
    clients.forEach(function (client) {
      if(client.socket == '') return;
      if (client === sender) return;
      client.write(message);
    });
    process.stdout.write(message+"\n")
  }
  
  exports.broadcastCanal = function(message, sender, clients){
    clients.forEach(function (client) {
      var canal = sender.canal;
      if(client.canal == '') return;
      if (client.canal == canal) {
         client.write(message +"\r\n");
      }
    });
    process.stdout.write(message+"\n")
  }

  exports.sendMessage = function(message, sender){
    sender.write(":p1-g7.localhost " + sender.nick + " :" + message + "\r\n");
  }  

  exports.sendServeMessage = function(message, err, sender){
    sender.write(":p1-g7.localhost " + err + " " +  sender.nick + " :" + message + "\r\n");
  }  
