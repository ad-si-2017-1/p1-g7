net = require('net');
names = require('../comandos/names');
priv = require('../comandos/priv');
time = require('../comandos/time');
quit = require('../comandos/quit');
nick = require('../comandos/nick');
join = require('../comandos/join');
user = require('../comandos/user');
userip = require('../comandos/userip');
utils = require('../utils/messages');
var clients = [];
var nicks = {}; //array responsável pelo armazenamento dos nicknames
var canais = ['#ad-si-2017-1'];
net.createServer(function (socket) {
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  socket.nick = "";
  socket.canal = '';
  socket.user =  '';
  clients.push(socket);
  socket.on('data', function (data) {
    var args = data.toString("utf-8").trim().split(' ');
    switch(args[0].toUpperCase()){ //switch principal. Verifica os comandos enviados
      case "USER":
        user.user(socket, args);
        break;
      case "JOIN":
        join.join(socket, canais, nicks, args);
        break;
      case "NICK": //comando para mudar o nick, use NICK <nickname>
        nick.nick(socket, nicks, args);
        break;
      case "USERIP": //comando para saber o ip de um usuário. Use: USERIP <nickname>
         userip.userip(socket, nicks, args);
         break;
      case "QUIT": //comando para se desconectar. Deleta o usuário da array associativa.
        quit.quit(socket, nicks, clients);     
        break;
      case "NAMES": //comando para receber nicknames dos usuários conectados. 
        names.names(nicks, socket);
        break;
      case "PRIVMSG": //comando para mandar msg privada. Use: PRIVMSG USER :MESSAGE
        priv.sendPriv(socket,  data.toString("utf-8"),
           data.toString("utf-8").split(" ")[1], clients);
        break;
      case "LIST": //comando que envia lista de canais no servidor.
        var canal = ''; 
        for (i = 0; i < canais.length; i++) {
          utils.sendMessage(canais[i], socket);
        }
        break;
      case "TIME":
        socket.write(":p1-g7.localhost 391 " + socket.nick + " p1-g7.localhost :" + time.parseTime()+"\r\n");
        break;
      default: 
        utils.sendServeMessage(args[0], "421", socket);
        break;
    }
    console.log(data.toString("utf-8"));
  });
 
  socket.on('end', function () {
        clients.splice(clients.indexOf(socket), 1);
  });
}).listen(6667);
console.log("Rodando na porta 6667\n");
