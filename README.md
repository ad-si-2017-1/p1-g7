PROJETO  - APLICAÇÕES DISTRIBUÍDAS DO GRUPO 7

MEMBROS:
    * ALLEF SILVA
    * BRUNA CARNEIRO
    * EDUARDO LAPA
    * ISRAEL ALVES
    * JESSICA MILENE

# INFORMAÇÕES IMPORTANTES:
    
        > O projeto é voltado para a disciplina de Aplicações Distribuídas-UFG;
    
        > A contribuição dos participantes será fundamental para ter aprovação
        na mesma;

        > Em caso de dúvidas, por favor entrar em contato com a Líder 
        (Bruna Carneiro);

        > Temos monitor na discplina, ele também poderá ser acionado caso
        precise;

### TECNOLOGIAS UTILIZADAS:

        - [Máquina Virtual - Ubunto]- Disponibilizada pelo o professor no moodle
        - [Sockets]- Material disponibilizado no moodle
        - [Nodes.js] - Ferramenta de apoio - (Servidor)
        - [NPM]- Ferramenta de apoio - (Intalador de pacotes)
        - [Javascript-js] Linguagem de Programação
        - [GitLab] - Ferramenta de apoio
        - [RFC- 2812] - Documentação do Protocolo IRC
    

### INSTALAÇÃO:

    -[Nodes.js] (https://nodejs.org/en/) v.7.8 - Downloads
    
        *Instale as dependências e inicie o servidor 
    
    -[Chatzilla]: add-on IRC do Firefox

        * Depois de instalar, entrar no menu: Tools > Chatzilla

        * Para entrar em um canal, acessar no menu: Join Channel, escolher o 
        servidorfreenode e o canal, por exemplo "#ad-si-2016-2"

        * Caso queira entrar em um canal ou criar um novo, use o comando:

         >/join #ad-si-2016-2

        * Para trocar de nick:

             >/nick <novonickname>

        * Procure por comandos IRC:

            > http://www.prof2000.pt/users/lpitta/ajuda/mirc/lista_cmd_mirc.htm

            > http://www.pthelp.org/tutorial_comandos
            
       * Link contendo as mensagens do servidor IRC:
         > https://gist.github.com/Ferus/2632945     
### DESENVOLVIMENTO:

    >MÃOS À OBRA!!!
