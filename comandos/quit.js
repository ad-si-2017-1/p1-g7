var names = require("./names");
exports.quit = function(socket, nicks, clients){ 
  var nickname = "";
  var username = "";
  nickname = socket.nick;
  username = socket.user;
  delete nicks[socket.nick];
  clients.splice(clients.indexOf(socket), 1);
  names.names(nicks, socket);
  socket.destroy();        
}
