names = require("./names");
exports.join = function(socket, canais, nicks, args){
  if (canais.indexOf(args[1])>=0){
   socket.canal = args[1].trim();
   socket.write(":p1-g7.localhost 302 " + socket.nick +
   "!~" + socket.user + "@"+socket.remoteAddress +
   " JOIN " + args[1] + "\r\n");
   names.names(nicks, socket);
  }
}
